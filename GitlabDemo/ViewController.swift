//
//  ViewController.swift
//  GitlabDemo
//
//  Created by Денис Сидоренко on 29/08/2019.
//  Copyright © 2019 Денис Сидоренко. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("hello world")
        print("some code in master branch")
        print("some code in tusk1")
        print("one more code")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

}

